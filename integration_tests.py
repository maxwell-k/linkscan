# integration_tests.py
# Copyright 2023 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
import unittest
from subprocess import run
from sys import executable


class _Base(unittest.TestCase):
    def setUp(self) -> None:
        self.cmd = (executable, "-m", "linkscan")

    def stdout(self, input=None) -> str:
        """Return stdout for a subprocess.run of self.cmd"""
        completed = run(
            self.cmd,
            capture_output=True,
            check=True,
            text=True,
            input=input,
        )
        return completed.stdout


class InterfaceTests(_Base):
    """Check that click is configured properly"""

    def test_usage(self):
        completed = run(self.cmd, capture_output=True)
        self.assertEqual(2, completed.returncode)
        self.assertTrue(completed.stderr.startswith(b"Usage:"))

    def test_help_text(self):
        self.cmd += ("--help",)
        self.assertIn("linkscan", self.stdout())

    def test_help_error_if_no_arguments(self):
        completed = run(self.cmd, capture_output=True)
        self.assertEqual(2, completed.returncode)

    def test_help_error_if_file_no_present(self):
        self.cmd += ("not-a-file.md",)
        completed = run(self.cmd, capture_output=True)
        self.assertEqual(2, completed.returncode)

    def test_help_error_if_line_missing(self):
        self.cmd += ("fixture.md",)
        completed = run(self.cmd, capture_output=True)
        self.assertEqual(2, completed.returncode)

    def test_help_error_if_line_non_numeric(self):
        self.cmd += ("fixture.md", "a")
        completed = run(self.cmd, capture_output=True)
        self.assertEqual(2, completed.returncode)

    def test_version(self):
        self.cmd += ("--version",)
        self.assertIn("version 20", self.stdout())

    def test_stdin(self):
        self.cmd += ("-", "1")
        input_ = "<https://example.org>\n"
        self.assertEqual("https://example.org\n", self.stdout(input_))


class FunctionalityTests(_Base):
    """Check against fixture.md"""

    def test_line_1_empty(self):
        self.cmd += ("fixture.md", "1")
        self.assertEqual(0, len(self.stdout()))

    def test_line_3_example_org(self):
        self.cmd += ("fixture.md", "3")
        self.assertEqual("https://example.org\n", self.stdout())

    def test_line_7_example_org(self):
        self.cmd += ("fixture.md", "7")
        self.assertEqual("https://example.org\n", self.stdout())

    def test_line_9_example_com(self):
        self.cmd += ("fixture.md", "9")
        self.assertEqual("https://example.com\n", self.stdout())


if __name__ == "__main__":
    unittest.main()
