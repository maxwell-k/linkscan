# unit_tests.py
# Copyright 2023 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
import doctest
import unittest

from click.testing import CliRunner

import linkscan


def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite(linkscan))
    return tests


class RunnerTests(unittest.TestCase):
    """Following https://click.palletsprojects.com/en/8.1.x/testing/"""

    def test_linkscan_file(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            with open("hello.txt", "w") as f:
                f.write("http://example.org")

            result = runner.invoke(linkscan.main, ["hello.txt", "1"])
            self.assertEqual(result.exit_code, 0)
            self.assertEqual(result.output, "http://example.org\n")

    def test_linkscan_stdin(self):
        runner = CliRunner()
        text = "http://example.com\n"
        result = runner.invoke(linkscan.main, ["-", "1"], input=text)
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, "http://example.com\n")


class ApiTests(unittest.TestCase):
    def test_link(self):
        result = linkscan.linkscan("<http://example.org>\n", 1)
        self.assertEqual(result, "http://example.org")

    def test_todo(self):
        text = "- [ ] A\n- [ ] [B]\n\n[b]: https://example.org\n"
        self.assertEqual(linkscan.linkscan(text, 2), "https://example.org")

    def test_link_reference_definition(self):
        text = "[example]: https://example.org\n\n[example]\n"
        self.assertEqual(linkscan.linkscan(text, 1), "https://example.org")


if __name__ == "__main__":
    unittest.main()
