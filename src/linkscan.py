# linkscan.py
# Copyright 2023 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
"""Scan a markdown document for a link

>>> linkscan("Markdown link: <https://example.org>", 1)
'https://example.org'
"""
from typing import cast

import click
from markdown_it import MarkdownIt
from markdown_it.token import Token
from urlextract import URLExtract

__version__ = "2023.11.20"


def _parse(text: str, line: int) -> str | None:
    """Use MarkdownIt to obtain the first URL or None

    line is zero indexed

    >>> _parse("<http://example.org>", 0)
    'http://example.org'
    """
    md = MarkdownIt()
    tokens = md.parse(text)
    tokens = (i for i in tokens if i.map and i.map[0] <= line < i.map[1])
    tokens = (token for token in tokens if token.type == "inline")
    try:
        link = next(tokens)
    except StopIteration:
        return None

    children = cast(list[Token], link.children)

    try:
        child = next(child for child in children if child.attrs)
    except StopIteration:
        return None
    return cast(str | None, child.attrs.get("href", None))


def _extract(text: str) -> str | None:
    """Use urlextract to get the first URL from a string

    >>> _extract("http://example.org http://example.com")
    'http://example.org'
    """
    extractor = URLExtract()
    try:
        return cast(str | None, next(extractor.gen_urls(text)))
    except StopIteration:
        return None


def linkscan(markdown: str, line: int) -> str | None:
    """Parse a markdown link fall back to a URL

    line is one indexed

    >>> linkscan("<http://example.org>", 1)
    'http://example.org'
    >>> linkscan("http://example.org http://example.com", 1)
    'http://example.org'
    >>> linkscan(" ", 1) is None
    True
    """
    line -= 1  # switch from one-based indexing to zero-based
    if href := _parse(markdown, line):
        return href

    return _extract(markdown.splitlines()[line])


@click.command()
@click.version_option(__version__)
@click.argument("markdown", type=click.File())
@click.argument("line", type=int)
def main(markdown, line: int) -> None:
    text = markdown.read()

    if href := linkscan(text, line):
        print(href)


if __name__ == "__main__":
    main()
