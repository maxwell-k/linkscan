<!--
README.md
Copyright 2023 Keith Maxwell
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# README

_Scan a line in a markdown file for a link_

[`linkscan`] prints the first link from the specified line in a markdown file.
`linkscan` was inspired by [`urlscan`].

Example usage:

    linkscan README.md 5

Example output:

    https://gitlab.com/maxwell-k/linkscan

Command to install with [`uv`]:

    uv tool install --index-url https://gitlab.com/api/v4/projects/43703506/packages/pypi/simple linkscan

Command to install with [`pipx`]:

    pipx install --index-url https://gitlab.com/api/v4/projects/43703506/packages/pypi/simple linkscan

[`linkscan`]: https://gitlab.com/maxwell-k/linkscan
[`pipx`]: https://github.com/pypa/pipx
[`urlscan`]: https://github.com/firecat53/urlscan
[`uv`]: https://github.com/astral-sh/uv
