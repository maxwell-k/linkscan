# noxfile.py
# Copyright 2023 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
"""Install into .venv and run checks

Assumes the following is already installed:

- Python - https://www.python.org/
- Git - https://git-scm.com/
- Nox - https://nox.thea.codes/
"""
from pathlib import Path
from shutil import rmtree
from sqlite3 import connect
from tomllib import load

import nox

TESTS = [
    "unit_tests.py",
    "integration_tests.py",
]

VIRTUAL_ENVIRONMENT = Path(".venv")
PYTHON = VIRTUAL_ENVIRONMENT / "bin" / "python"


def _name() -> str:
    """Read the project name from pyproject.toml"""
    with open("pyproject.toml", "rb") as f:
        data = load(f)
    return data["project"]["name"]


@nox.session(python=False)
def dev(session) -> None:
    """Set up a development environment"""
    rmtree(VIRTUAL_ENVIRONMENT, ignore_errors=True)
    session.run(
        "python",
        "-m",
        "venv",
        "--upgrade-deps",
        VIRTUAL_ENVIRONMENT,
    )
    session.run(PYTHON, "-m", "pip", "install", "--editable=.[dev]")


@nox.session()
def reuse(session):
    session.install("reuse")
    session.run("reuse", "--version")
    session.run("reuse", "lint")


@nox.session()
def black(session):
    session.install("black")
    session.run("black", "--version")
    session.run("black", ".")


@nox.session()
def flake8(session):
    session.install("flake8")
    session.run("flake8", "--version")
    session.run("flake8", ".")


@nox.session()
def reorder(session):
    session.install("reorder-python-imports")
    session.run("pip", "show", "reorder-python-imports")
    output = session.run("git", "ls-files", "*.py", silent=True, external=True)
    session.run(
        "reorder-python-imports",
        "--unclassifiable-application-module={}".format(_name()),
        *output.splitlines(),
    )


@nox.session(python=False)
def pyright(session):
    for args in ("--version",), (f"--pythonpath={PYTHON}", *session.posargs, "."):
        session.run("npm", "exec", "--yes", "--", "pyright", *args, external=True)


@nox.session()
def tests(session):
    """Build and then test while measuring coverage

    1. build a wheel distribution
    2. install the wheel
    3. run tests against the installed files recording test coverage
    4. point the stored coverage data at the repository contents
    5. print a summary report to standard out
    6. write an html formatted report
    7. print a total to be displayed in GitLab

    Tests include doctests, unit tests and integration tests"""
    rmtree("dist", ignore_errors=True)
    session.install("build")
    session.run("python", "-m", "build")
    session.install("--find-links=./dist/", _name(), "coverage")
    session.run(
        "python",
        "-m",
        "coverage",
        "run",
        "--source={}".format(_name()),
        "-m",
        "unittest",
        "--verbose",
        *TESTS,
    )
    before = session.run(
        "python",
        "-c",
        "from sys import path; print(path[-1], end='')",
        silent=True,
    )
    after = Path("src").resolve()
    sql = f"UPDATE file SET path = replace(path, '{before}', '{after}');"
    con = connect(".coverage")
    con.execute(sql)
    con.commit()
    con.close()
    session.run("python", "-m", "coverage", "report")
    session.run("python", "-m", "coverage", "html")
    total = session.run(
        "python",
        "-m",
        "coverage",
        "report",
        "--format=total",
        silent=True,
    )
    session.log(f"Coverage total for GitLab: {total}")
    session.run("python", "-m", "coverage", "xml")
